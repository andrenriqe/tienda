package com.example.lenovo.tienda;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {
    private EditText username,contraseña;
    private String URL_login="http://168.62.42.172/ciudadmusical/comprobarUser/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
         username=(EditText)findViewById(R.id.usernamelogin);
         contraseña=(EditText)findViewById(R.id.Contrasenalogin);

    }
    public void Aceptarlogin(){
        final String usernamet=username.getText().toString();
        final String contraseñat=contraseña.getText().toString();
        StringRequest coneccion =new StringRequest(Request.Method.POST, URL_login, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.equals("0")) {
                    Toast.makeText(Login.this, "datos incorrecto", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        //datos del srvidor
                        JSONArray jsonArray = new JSONArray(response);
                        String nombre = jsonArray.getJSONObject(0).getString("username");
                        String contra = jsonArray.getJSONObject(0).getString("password");
                        Intent intent=new Intent(Login.this,PantallaPrincipal.class);
                        intent.putExtra("username",nombre);
                        intent.putExtra("password",contra);
                        startActivity(intent);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error){

                    }

            }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError{
                Map<String, String> parametrs = new HashMap<>();
                parametrs.put("username",usernamet);
                parametrs.put("password",contraseñat);

                return parametrs;
            }
        };
        Volley.newRequestQueue(this).add(coneccion);

    }
}
